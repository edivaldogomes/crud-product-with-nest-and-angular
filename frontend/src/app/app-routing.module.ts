import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListAllComponent } from './components/product/list-all/list-all.component';
import { CreateComponent } from './components/product/create/create.component';
import { UpdateComponent } from './components/product/update/update.component';
import { DetailComponent } from './components/product/detail/detail.component';

const routes: Routes = [
  { path: '', component: ListAllComponent },
  { path: 'detalle/:id', component: DetailComponent },
  { path: 'nuevo', component: CreateComponent },
  { path: 'editar/:id', component: UpdateComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
