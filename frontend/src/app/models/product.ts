export class Product {
  id?: number;
  name?: string;
  price?: number;
  message?: string;

  constructor(name: string, price: number) {
    this.name = name;
    this.price = price;
  }
}
