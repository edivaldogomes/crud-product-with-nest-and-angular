import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/service/product.service';
import { faEye, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-all',
  templateUrl: './list-all.component.html',
  styleUrls: ['./list-all.component.css'],
})
export class ListAllComponent implements OnInit {
  faEye = faEye;
  faEdit = faEdit;
  faTrash = faTrash;
  products: Product[] = [];
  emptyList: undefined;

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.loadProducts();
  }

  loadProducts() {
    this.productService.listAll().subscribe(
      (data) => {
        this.emptyList = undefined;
        this.products = data;
      },
      (err) => {
        this.emptyList = err.error.message;
        console.log(err);
      }
    );
  }
  delete(id: number | undefined): void {
    if (id) {
      Swal.fire({
        title: '¿Está seguro?',
        text: 'No hay vuelta atrás',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'Nops',
      }).then((result) => {
        if (result.value) {
          this.productService.delete(id).subscribe((res) => {
            window.location.reload();
            this.productService.listAll();
          });
          Swal.fire(`OK`, `Producto eliminado`, `success`);
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire(`Cancelado`, `Producto a salvo`, `error`);
        }
      });
    }
  }
}
