import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/service/product.service';
import { faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
})
export class DetailComponent implements OnInit {
  product?: Product;
  faArrowCircleLeft = faArrowCircleLeft;

  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit(): void {
    const id: number = this.activatedRoute.snapshot.params['id'];
    this.productService.detail(id).subscribe(
      (data) => {
        this.product = data;
      },
      (err) => {
        this.toastr.error(err.error.menssage, 'Fail', {
          timeOut: 3000,
          positionClass: 'toast-top-center',
        });
        this.goBack();
      }
    );
  }

  goBack(): void {
    this.router.navigate(['/']);
  }
}
