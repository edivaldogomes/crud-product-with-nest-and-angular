import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/service/product.service';
import { faEdit, faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css'],
})
export class UpdateComponent implements OnInit {
  faEdit = faEdit;
  faArrowCircleLeft = faArrowCircleLeft;

  product!: Product;

  constructor(
    private productService: ProductService,
    private router: Router,
    private activactedRouter: ActivatedRoute,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    const id = this.activactedRouter.snapshot.params['id'];

    this.productService.detail(id).subscribe(
      (data) => {
        this.product = data;
      },
      (err) => {
        this.toastr.error(err.error.message, 'Fail', {
          timeOut: 3000,
          positionClass: 'toast-top-center',
        });
        this.router.navigate(['/']);
      }
    );
  }

  onUpdate() {
    const id = this.activactedRouter.snapshot.params['id'];
    this.productService.update(id, this.product).subscribe(
      (data) => {
        this.toastr.success(data.message, 'Ok', {
          timeOut: 3000,
          positionClass: 'toast-top-center',
        });
        this.router.navigate(['/']);
      },
      (err) => {
        this.toastr.error(err.error.message, 'Fail', {
          timeOut: 3000,
          positionClass: 'toast-top-center',
        });
      }
    );
  }

  goBack() {
    this.router.navigate(['/']);
  }
}
