import { Component } from '@angular/core';
import {
  faCoffee,
  faPlusCircle,
  faList,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  faCoffee = faCoffee;
  faPlusCircle = faPlusCircle;
  faList = faList;
  title = 'frontend';
}
