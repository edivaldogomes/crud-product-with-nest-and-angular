import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  productUrl = environment.productURL;

  constructor(private http: HttpClient) {}

  public listAll(): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.productUrl}`);
  }

  public detail(id: number): Observable<Product> {
    return this.http.get<Product>(`${this.productUrl}${id}`);
  }

  public detailName(name: string): Observable<Product> {
    return this.http.get<Product>(`${this.productUrl} ${name}`);
  }

  public save(product: Product): Observable<Product> {
    return this.http.post<Product>(`${this.productUrl}`, product);
  }

  public update(id: number, product: Product): Observable<Product> {
    return this.http.put<any>(`${this.productUrl}${id}`, product);
  }

  public delete(id: number): Observable<any> {
    return this.http.delete<any>(`${this.productUrl}${id}`);
  }
}
